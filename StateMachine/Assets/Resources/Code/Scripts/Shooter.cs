﻿using UnityEngine;
using System.Collections;
using Assets.Resources.Code.Scripts;

public class Shooter : MonoBehaviour
{

    // hold a reference to a physics controlled object we will make
    public Rigidbody bullet;

    //  use to s et the power of shooting
    public float power = 1500f;

    //define the speed of movement of the camera 
    public float moveSpeed = 2f;

    // reference to active camera
    public Camera myCamera;

    // reference to data of game
    private GameData gameDataRef;

    private static Shooter matShooter;

    /// <summary>
    /// Use this for initialization
    /// </summary>
    /// <returns></returns>
	void Start () {

        // assign GameData to gameDataRef variable
        gameDataRef = GameObject.Find("GameManager").GetComponent<GameData>();

        matShooter = this;
	}

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    /// <returns></returns>
	public void UpdateShooter ( bool fireState ) {

        // if fire state =  true shoot a sphere
        if (fireState)
        {
            // transform mouse position form screen to camera
            Ray ray = myCamera.ScreenPointToRay(Input.mousePosition);

            // drawing green line from ray orignin, in ray deirection, of force value
            Debug.DrawRay(ray.origin, ray.direction * gameDataRef.forceManager.actualForce, Color.green);

            //instantiate a new sphere assign it to bullet and positioning it in the scene
            Rigidbody sphereInstance = Instantiate(bullet, ray.origin, transform.rotation) as Rigidbody;

            // reference to the direction forward of camera ( really to the object which the script is attached and in our case camera )
            Vector3 forwardCameraDirection = transform.TransformDirection(Vector3.forward);

            Debug.LogWarning("Fire Force value:= " + gameDataRef.forceManager.actualForce);
            // adding force to the sphere instance
            sphereInstance.AddForce(ray.direction * gameDataRef.forceManager.actualForce);
        }
	}
}